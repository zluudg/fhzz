[Unreleased]
===
- Flag `-t` can now handle both https and http schemes
- Expect `{{FUZZ}}` as placeholder instead of `REPLACEME`
- Flag `-w` can now be a directory, which will treat its contents as binary
  files and use those instead of a wordlist

[0.1.0] - 2023-07-14
===
- Send garbage data over ssl socket without crashing too often
