"""
Provide a command line interface for fhzz.
"""

from fhzz.main import main

__all__ = ("main",)

if __name__ == "__main__":
    main()
