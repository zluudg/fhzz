"""
Where the work takes place.
"""

import copy
import os
import os.path
import pprint
import socket
import ssl


def _read_wordlist(wordlist_path):
    words = []
    if not wordlist_path:
        return words
    elif os.path.isfile(wordlist_path):
        with open(wordlist_path) as f:
            words = [_encode(line.strip()) for line in f.read().splitlines()
                     if not line.startswith("#")]
    elif os.path.isdir(wordlist_path):
       for file in os.listdir(wordlist_path):
            fname = os.path.join(wordlist_path, os.fsdecode(file))
            with open(fname, mode='rb') as f:
                words.append(f.read())
    return words


def _read_req(request_path):
    headers = {}
    with open(request_path) as f:
        start_line = _encode(f.readline().strip())
        for line in f.read().splitlines():
            key, value = line.split(":", 1)
            headers.update({_encode(key): _encode(value.strip())})
    return start_line, headers


def _dummy_socket_wrap(socket, server_hostname=None):
    return socket


def _encode(s):
    return str.encode(s)


PLACEHOLDER = _encode("{{FUZZ}}")


def start(request, header, target, wordlist_path=None):
    wordlist = _read_wordlist(wordlist_path)
    start_line_orig, req_headers_orig = _read_req(request)
    header = _encode(header)

    context = ssl.create_default_context()
    context.check_hostname = 0
    context.verify_mode = ssl.CERT_NONE

    scheme = "http"
    host = "localhost"
    port = 80
    socket_wrap = _dummy_socket_wrap
    if "://" in target:
        scheme, host = target.split("://")
    if ":" in host:
        host, port = host.split(":")
    if "https" == scheme or 443 == port:
        socket_wrap = context.wrap_socket
    port = int(port)

    for i, word in enumerate(wordlist):
        req_headers = copy.deepcopy(req_headers_orig)
        start_line = start_line_orig
        if PLACEHOLDER in req_headers[header]:
            req_headers[header] = req_headers[header].replace(PLACEHOLDER, word)
        elif PLACEHOLDER in start_line:
            start_line = start_line.replace("REPLACEME", word)
        else:
            req_headers[header] = word
        data = start_line + _encode("\r\n")
        for key, val in req_headers.items():
            data += key
            data += _encode(": ")
            data += val
            data += _encode("\r\n")
        data += _encode("\r\n")

        conn = socket_wrap(socket.socket(socket.AF_INET),
                           server_hostname=host)
        conn.connect((host, port))
        conn.sendall(data)

        print(f"########### Test case {i} ###########")
        print("###### SENT")
        pprint.pprint(data.split(b"\r\n"))
        print("###### RECEIVED")
        pprint.pprint(conn.recv(4096).split(b"\r\n"))
        conn.close()
        print("################ END ################")
